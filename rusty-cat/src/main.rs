#[macro_use]
extern crate clap;

use std::fs::File;
use std::io;
use std::io::prelude::*;

fn main() -> Result<(), io::Error>{
    let matches = clap_app!(rustycat =>
        (version: "0.1.0")
        (author: "Tyler Goodwin")
        (about: "Attempt at a Cat clone")
        (@arg unbuffered: -u ... "Do not buffer input")
        (@arg INPUT: +required ... "Sets the input files to use")
    ).get_matches();

    let files = matches.values_of("INPUT").unwrap();
    let unbuffered = matches.is_present("unbuffered");

    for file in files {
        match file {
            "-" => read_and_output(io::stdin(), unbuffered),
            _ => read_and_output(File::open(file)?, unbuffered)
        }?;
    }
    Ok(())
}


fn read_and_output<R: Read>(mut input: R, unbuffered : bool) -> Result<(), io::Error>{
    if unbuffered {
        for byte in input.bytes() {
            let bytes = [byte.unwrap()];
            io::stdout().write(&bytes)?;
        }
    }
    else {
        let mut buffer = String::new();
        input.read_to_string(&mut buffer)?;
        print!("{}", buffer);
    }
    Ok(())
}